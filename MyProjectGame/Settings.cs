﻿namespace MyProjectGame
{
    class Settings
    {
        public string firstClassName = "Warrior";
        public int warriorHp = 100;
        public int warriorDefens = 3;
        public int warriorMinDmg = 5;
        public int warriorMaxDmg = 10;
        public int warriorAbilityOne = 10;
        public int warriorAbilityTwo = 1;
        public int warriorAbilityTree = 10;

        public string secondClassName = "Mage";
        public int mageHp = 70;
        public int mageDefens = 3;
        public int mageMinDmg = 5;
        public int mageMaxDmg = 10;
        public int mageAbilityOne = 10;
        public int mageAbilityTwo = 2;
        public int mageAbilityTree = 5;

        public string thirdClassName = "Rogue";
        public int rogueHp = 100;
        public int rogueDefens = 3;
        public int rogueMinDmg = 5;
        public int rogueMaxDmg = 10;
        public int rogueAbilityOne = 10;
        public int rogueAbilityTwo = 2;
        public int rogueAbilityTree = 5;
    }
}
