﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace MyProjectGame
{
    class Program
    {
        static void Main(string[] args)
        {
            Settings settings = new Settings();
            GameInterface gameInterface = new GameInterface();
            PlayerCharacter playerCharacter = new PlayerCharacter("2", "3", 0, 0, 0, 0, 0, 0, 0); // костыль, как и три экземпляра ниже =(
            EnemyCharacter enemyCharacter = new EnemyCharacter("Zlo", 0, 0, 0, 0, 0);

            PlayerCharacter warriorCharacter = new PlayerCharacter("Ivannnn", settings.firstClassName, settings.warriorHp,
                        settings.warriorDefens, settings.warriorMinDmg, settings.warriorMaxDmg, settings.warriorAbilityOne,
                        settings.warriorAbilityTwo, settings.warriorAbilityTree);

            PlayerCharacter mageCharacter = new PlayerCharacter("Ivannnn", settings.secondClassName, settings.mageHp,
                        settings.mageDefens, settings.mageMinDmg, settings.mageMaxDmg, settings.mageAbilityOne,
                        settings.mageAbilityTwo, settings.mageAbilityTree);

            PlayerCharacter rogueCharacter = new PlayerCharacter("Ivannnn", settings.thirdClassName, settings.rogueHp,
                        settings.rogueDefens, settings.rogueMinDmg, settings.rogueMaxDmg, settings.rogueAbilityOne,
                        settings.rogueAbilityTwo, settings.rogueAbilityTree);

            int userResponse = gameInterface.CustomMenu("Choose your character class: ", "Warrior", "Mage", "Roge");

            switch (userResponse)
            {
                case 0:
                    playerCharacter = warriorCharacter;
                    break;

                case 1:
                    playerCharacter = mageCharacter;
                    break;

                case 2:
                    playerCharacter = rogueCharacter;
                    break;
            }

            Console.Clear();
            gameInterface.ShowGameCharacteristics(playerCharacter.name, playerCharacter.playerClass, playerCharacter.lvl,
                playerCharacter.hp, playerCharacter.defens, playerCharacter.minDmg, playerCharacter.maxDmg,
                playerCharacter.abilityOne, playerCharacter.abilityTwo,playerCharacter.abilityTree, 
                enemyCharacter.name, enemyCharacter.hp, enemyCharacter.defens, enemyCharacter.minDmg,
                enemyCharacter.maxDmg, enemyCharacter.abilityOne);

            //playerCharacter.PlayerCharacterLvlUp(playerCharacter.playerClass, ref playerCharacter.lvl,
            //    ref playerCharacter.hp, ref playerCharacter.defens, ref playerCharacter.minDmg,
            //    ref playerCharacter.maxDmg, ref playerCharacter.abilityOne, ref playerCharacter.abilityTwo, 
            //    ref playerCharacter.abilityTree);
            
            Console.ReadKey();
        }
    }
}
