﻿namespace MyProjectGame
{
    public abstract class Character
    {
        public string name;
        public int hp;
        public int lvl = 1;
        public int defens;
        public int minDmg;
        public int maxDmg;
        public int abilityOne;


        public Character(string name, int hp, int defens, int minDmg, int maxDmg, int abilityOne)
        {
            this.name = name;
            this.hp = hp;
            this.defens = defens;
            this.minDmg = minDmg;
            this.maxDmg = maxDmg;
            this.abilityOne = abilityOne;
        }
    }
}