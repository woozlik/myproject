﻿using System;

namespace MyProjectGame
{
    class GameInterface
    {

        public int CustomMenu(string message, string firstOption, string secondOption, string thirdOption)
        {
            int userChoice = -1;
            int activeOption = 0;

            do
            {
                switch (activeOption)
                {
                    case 0:
                        Console.Clear();
                        Console.WriteLine($"{message}\n");
                        Console.BackgroundColor = ConsoleColor.Red;
                        Console.ForegroundColor = ConsoleColor.White;
                        Console.Write($"{firstOption}  ");
                        Console.ResetColor();
                        Console.Write($"   {secondOption}   ");
                        Console.Write($"   {thirdOption}   ");
                        break;

                    case 1:
                        Console.Clear();
                        Console.WriteLine($"{message}\n");
                        Console.Write($"{firstOption}  ");
                        Console.BackgroundColor = ConsoleColor.Red;
                        Console.ForegroundColor = ConsoleColor.White;
                        Console.Write($"   {secondOption}   ");
                        Console.ResetColor();
                        Console.Write($"   {thirdOption}   ");
                        break;

                    case 2:
                        Console.Clear();
                        Console.WriteLine($"{message}\n");
                        Console.Write($"{firstOption}  ");
                        Console.Write($"   {secondOption}   ");
                        Console.BackgroundColor = ConsoleColor.Red;
                        Console.ForegroundColor = ConsoleColor.White;
                        Console.Write($"   {thirdOption}   ");
                        Console.ResetColor();
                        break;
                }

                var playerChoice = Console.ReadKey().Key;

                switch (playerChoice)
                {
                    case ConsoleKey.Enter:
                        userChoice = activeOption;
                        break;

                    case ConsoleKey.RightArrow:
                        activeOption += 1;
                        break;

                    case ConsoleKey.LeftArrow:
                        activeOption -= 1;
                        break;
                }

                if (activeOption > 2)
                {
                    activeOption = 0;
                }

                if (activeOption < 0)
                {
                    activeOption = 2;
                }

            }
            while (userChoice == -1);

            return userChoice;
        }

        public void ShowGameCharacteristics(string playerName, string playerClass, int lvl, int playerHp,
            int playerDefens, int playerMinDmg, int playerMaxDmg, int playerAbilityOne, int playerAbilityTwo, 
            int playerAbilityTree, string enemyName, int enemyHp, int enemyDefens, int enemyMinDmg,
            int enemyMaxDmg, int enemyAbility)
        {
            int columnLength = 15;
            int numberOfSpaces;
            string displayString = ($"{playerName} {playerClass}");

            NumberOfSpaces(displayString, columnLength, out numberOfSpaces);
            string spaces = new string(' ', numberOfSpaces);
            Console.WriteLine($"{displayString}{spaces}  VS {new string(' ', 5)}{enemyName} ");

            spaces = new string(' ', 7);
            Console.WriteLine($"{spaces}LVL{lvl}{spaces}|{spaces}LVL{lvl}");

            //NumberOfSpaces("Health: ", $"{playerHp}", columnLength, out numberOfSpaces);
            //spaces = new string(' ', numberOfSpaces);

            //Console.WriteLine($"Health: {playerHp}{spaces}|");
        }

        private void NumberOfSpaces(string displayString, int columnLength, out int numberOfSpaces)
        {
            numberOfSpaces = columnLength - displayString.Length;
        }

        private void NumberOfSpaces(string firstString, string secondString, int columnLength, out int numberOfSpaces)
        {
            numberOfSpaces = columnLength - (firstString.Length + secondString.Length);
        }
    }
}
