﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyProjectGame
{
    class PlayerCharacter : Character
    {
        public string playerClass;
        public int abilityTwo;
        public int abilityTree;
        

        public PlayerCharacter(string playerName, string playerClass, int hp, int defens, int minDmg, int maxDmg,
            int abilityOne, int abilityTwo, int abilityTree) : base(playerName, hp, defens, minDmg, maxDmg, abilityOne)
        {
            this.playerClass = playerClass;
            this.abilityTwo = abilityTwo;
            this.abilityTree = abilityTree;
        }

        public void PlayerCharacterLvlUp(string playerClass, ref int lvl, ref int hp, ref int defens, ref int minDmg,
           ref int maxDmg, ref int abilityOne, ref int abilityTwo, ref int abilityTree)
        {
            GameInterface gameInterface = new GameInterface();

            switch (playerClass)
            {
                case "Warrior":
                    lvl = lvl + 1;
                    hp = +10;
                    defens = +1;
                    minDmg = +2;
                    maxDmg = +2;
                    int userResponse = gameInterface.CustomMenu("Select skill to upgrade: ", "1. Hero strike",
                        "2. Protective strike", "3. Block chance");
                    switch (userResponse)
                    {
                        case 0:
                            abilityOne = +3;
                            break;
                        case 1:
                            abilityTwo = +1;
                            break;
                        case 2:
                            abilityTree = +5;
                            break;
                    }
                    break;
            }
        }
    }
}

